#!/usr/bin/env bash
#
for host in orbit-build-22-04-amd mms-dev orbit-build-23-04-amd orbit-build-24-04-amd orbit-dev
do
	ssh ${host} -- "source .bash_profile;cd scm/orb;git pull;nvm use lts/hydrogen;pyenv activate orbit;make all"
	rsync -v ${host}:scm/orb/archive/* releases
done
