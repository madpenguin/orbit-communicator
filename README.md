# Orbit Communicator!

## Description

Orbit Communicator is a client server application that's designed to be deployed locally on your desktop machine. It currently provides an 
integrated email / calendaring experience based on external IMAP mail sources (which are treated as read-only). It can also be run inside
a virtual machine, and if you need remote access works well via Cloudflare's Zero Trust network / free tier..

Currently for the server side (i.e. your desktop or virtual machine) you will need Ubuntu 22.04, and for the client side you will need
either Google Chrome or Chromium browsers. (others should work but are untested)

![Screenshots](https://gitlab.com/madpenguin/orbit-communicator/-/raw/8b593e2d1c8366d13eb7be1bf1d486aa17f6a10c/screenshots/screen1s.png "Screenshots")

## Information

Source Code for Orbit Communicator will eventually be deployed here. In the meantime, please use the "Issues" section if you would like to provide any feedback or report any bugs.

## Downloads

Please see the downloads section on the [Orbit Website](https://orbit.madpenguin.uk)

## Installation

Once you have downloaded the appropriate package, the following instructions apply;

On Linux;
```bash
apt install ./(package-file-name)
```

## Authors and acknowledgment

This project sits on top of many other Open Source projects without which this software would not be possible. Many thanks to all authors and
contributors responsible for making this all possible.

*If I have seen further, it is by standing on the shoulders of giants - Isaac Newton*

## License

All the constituent parts of the project are open MIT/BSD/Apache style licenses and required documentation files included in the download package
and can be found in /opt/orbit-communicator. These are automatically generated from the source code so if anyone sees anything missing or that
looks problematic, please file an issue and we'll look at it asap.

## Status

This software is BETA, so although people are using it, please be aware it is still subject to a number of bugs so use at your own risk...
